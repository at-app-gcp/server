import requests
import sys, os
sys.path.insert(0, os.path.abspath('..'))
import platform
import psutil
from datetime import datetime
import pandas as pd
from pathlib import Path
import numpy as np
import json
def terminate_loop (when):
    if when == "market_closing":
        pass

    if when == "now":
        return True
    if when == "never":
        return False
def log_system_info():

    __platform = platform.architecture()
    __cpu = platform.processor()
    __cpu_usage = psutil.cpu_percent()
    __cpu_times= psutil.cpu_times(percpu=False)
    __cpu_count = psutil.cpu_count(logical=True)
    __cpu_stats = psutil.cpu_stats()
    __cpu_freq = psutil.cpu_freq(percpu=False)
    # __load_avg = psutil.getloadavg()
    # __memory = dict(psutil.virtual_memory()._asdict())
    # __memory_used = psutil.virtual_memory().percent
    # __memory_free = psutil.virtual_memory().available * 100 / psutil.virtual_memory().total
    # __memory_swap = psutil.swap_memory()
    # __disk = psutil.disk_partitions()
    # __disk_bytes = psutil.disk_usage('/')
    # __disk_io = psutil.disk_io_counters(perdisk=False, nowrap=True)
    # __net_io_ = psutil.net_io_counters(pernic=False, nowrap=True)
    # __net_conn_len = len(psutil.net_connections(kind='inet'))
    # __boot_time = psutil.boot_time()
    # __users = psutil.users()
    # __processes_len = len(psutil.pids())

    payload = {
        "platform" : __platform,
        "cpu" : __cpu,
        "cpu_usage" :  __cpu_usage,
        "cpu_times" : __cpu_times,
        "cpu_count" : __cpu_count,
        "cpu_stats" : __cpu_stats,
        "cpu_freq" : __cpu_freq,
        # "load_avg" : __load_avg,
        # "memory" : __memory,
        # "memory_used" : __memory_used,
        # "memory_free" : __memory_free,
        # "memory_swap" : __memory_swap,
        # "disk" : __disk,
        # "disk_bytes" : __disk_bytes,
        # "disk_io" : __disk_io,
        # "net_io_" : __net_io_,
        # "net_conn_len" : __net_conn_len,
        # "boot_time" : __boot_time,
        # "user" : __users,
        # "processes_len" : __processes_len,
    }
    return payload

async def oanda_update_candles ():
    requests.get("https://europe-west3-at202xcloud.cloudfunctions.net/oanda_update_candles")
async def oanda_update_trades ():
    requests.get("https://europe-west3-at202xcloud.cloudfunctions.net/oanda_update_trades")
async def oanda_update_account ():
    requests.get("https://europe-west3-at202xcloud.cloudfunctions.net/oanda_update_account")

p ={
       "units" : "420",
       "stopLossOnFill": {
         "timeInForce": "GTC",
         "price": "1.2280"
       },
       "takeProfitOnFill": {
         "price": "1.2325"
       },
       "timeInForce": "FOK",
       "instrument": "EUR_USD",
       "type": "MARKET",
       "positionFill": "DEFAULT"
     }

async def oanda_create_order (payload):

    r = requests.post("https://europe-west3-at202xcloud.cloudfunctions.net/oanda_create_order",payload)
        
# oanda_create_order(p)

def oanda_get_trade(id):
    p = str(id)
    r = requests.post("https://europe-west3-at202xcloud.cloudfunctions.net/oanda_get_trade",{"tradeID" : p})
    if r.status_code == 200:
        return json.dumps(r.text())
    else:
        oanda_get_trade(p)

        
def create_workers_pool(instruments,time_frames,worker):

    instances = []
    # call function iwhich create boiler plate for all isntruments in cloud database
    # IMPORTNAT 

    for idx, inst  in enumerate(instruments):
        for tf in time_frames:
                instances.append(worker(instrument=inst,time_frame=tf, index=idx)) 

    return instances


