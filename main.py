from time import sleep
import requests
import asyncio
from  datetime import datetime
import concurrent.futures
import math
from worker import Worker
from api import firestore_db as db
from helpers import create_workers_pool
broker = db.collection("SYSTEM").document("broker").set({
    "instruments": ["EUR_AUD", "GBP_USD","AUD_USD", "EUR_USD","USD_CHF"],
    "time_frames": ["M1", "M15","H1"],
    # "instruments" : ["EUR_USD"],
    # "time_frames" : ["M1"],
    "candles_count": 20,
    "workers_count" : 15
})

db.collection("SYSTEM").document("server").set({"program_start_time" : datetime.now()})

workers = create_workers_pool(
    instruments= db.collection("SYSTEM").document("broker").get().to_dict()["instruments"],
    time_frames=db.collection("SYSTEM").document("broker").get().to_dict()["time_frames"],
    worker=Worker
)



async def main2():
    while True:
        for worker in workers:
            asyncio.ensure_future(worker.run())
        await asyncio.sleep(10)

def main():
    while True:
        for worker in workers:
            worker.run()
        sleep(10)

if __name__ == "__main__":
    # asyncio.run(main())
    main()