import json
import requests

import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
from firebase_admin import db 
 
# cred = requests.get("https://europe-west3-at202xcloud.cloudfunctions.net/FIRESTORE_get_JSONcredentials") 
firebase_app = firebase_admin.initialize_app(credentials.Certificate())
firestore_db = firestore.client()
realtime_db = db
