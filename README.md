*******
at-app/
    client/
        README.md
    functions/
        README.md
    iaac/
        README.md
    server/
        --> README.md 
*********





# Install Stackdriver logging agent
curl -sSO https://dl.google.com/cloudagents/install-logging-agent.sh
sudo bash install-logging-agent.sh

# Install or update needed software
apt-get update
apt-get install -yq git supervisor python python-pip
pip install --upgrade pip virtualenv

# Account to own server process
useradd -m -d /home/pythonapp pythonapp

# Fetch source code
export HOME=/root
git clone https://github.com/GoogleCloudPlatform/getting-started-python.git /opt/app

# Python environment setup
virtualenv -p python3 /opt/app/gce/env
source /opt/app/gce/env/bin/activate
/opt/app/gce/env/bin/pip install -r /opt/app/gce/requirements.txt

# Set ownership to newly created account
chown -R pythonapp:pythonapp /opt/app

# Put supervisor configuration in proper place
cp /opt/app/gce/python-app.conf /etc/supervisor/conf.d/python-app.conf

# Start service via supervisorctl
supervisorctl reread
supervisorctl update

# Install monitoring-agent logging agent for RAM and Disk
curl -sSO https://dl.google.com/cloudagents/add-monitoring-agent-repo.sh
sudo bash add-monitoring-agent-repo.sh
sudo apt-get update
sudo apt-cache madison stackdriver-agent
sudo apt-get install -y 'stackdriver-agent=6.*'
sudo service stackdriver-agent start
sudo service stackdriver-agent status

# Install Docker
sudo apt-get update
sudo apt-get install apt-transport-https ca-certificates curl gnupg2 software-properties-common
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add –
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian buster stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo systemctl status docker
docker -v

gcloud auth login
gcloud auth configure-docker
docker pull gcr.io/at202xcloud/at-server:latest

# writes : 7,152 20:50
        #  8,232 21:00
        #  9,411 21:11
        #  10,382 21:20
        #  12,530 21:40
        #  13,595 21:50
# 100 in 1 min
# 1080 in 10 mins 
# 3 230 in 30 mins
# 6 443 - 6 460 in 60 mins 

test
~150k writes for 24 workers for 24 hours per 10secs/loop 
$0.06 per 100,000 documents


# 24 workers n document fields writes every 10 seconds in 120h =  775 200 - 777 600 writes
#  777 600 - 100 000 (5 days free quota 20k)
# $0.117 per 100,000 documents
# 0,909792 $ for 777 600 writes
# 0,792792 $ for 677 600 writes = 120h x 10secs/loop - 5 days free quota 20k (-100k)

# max loop iters for 120 h @ 10 secs/loop = 43 200

# 10 secs/loop
# 4,4 times loop execution per minute ; max:6  
# 268,45 times loop execution per hour ; max:360
# 6 442 times loop execution per day : max 8640 
# 32215    times loop execution per 120 hours; max:43 200
# 128860 times loop execution per 120 hours x 4; max:172 800

# Net price for 24 workers 10 sec/loop
# 0,792792 $ @ one week (120 h) @ 32,215 executions
# 3,171168 $ @ one month (120h * 4) @ 128,860 executions

# Net price for 240 workers 10 sec/loop   
# 7,92792 $ @ one week (120 h); 322,150
# 31.71168 $ @ one month (120h * 4)  1,288,600


#  For writes, each set or update operation counts as a single write.



https://www.mattzeunert.com/2019/11/25/understanding-my-october-google-cloud-bill.html

36.66 @ 17:46


Task exception was never retrieved
future: <Task finished name='Task-6541' coro=<Worker.run() done, 
defined at C:\Users\fvrlak\Desktop\at-app-gcp\server\worker\__init__.py:152> exception=JSONDecodeError('Expecting value: line 1 column 1 (char 0)')>
Traceback (most recent call last):
  File "C:\Users\fvrlak\Desktop\at-app-gcp\server\worker\__init__.py", line 159, in run
    self.__apply_trading()
  File "C:\Users\fvrlak\Desktop\at-app-gcp\server\worker\__init__.py", line 143, in __apply_trading
    trade = json.loads(oanda_get_trade(self.tradeID))
  File "C:\Program Files\Python38\lib\json\__init__.py", line 357, in loads
    return _default_decoder.decode(s)
  File "C:\Program Files\Python38\lib\json\decoder.py", line 337, in decode
    obj, end = self.raw_decode(s, idx=_w(s, 0).end())
  File "C:\Program Files\Python38\lib\json\decoder.py", line 355, in raw_decode
    raise JSONDecodeError("Expecting value", s, err.value) from None
json.decoder.JSONDecodeError: Expecting value: line 1 column 1 (char 0)


Task exception was never retrieved
future: <Task finished name='Task-5665' coro=<Worker.run() done, defined at C:\Users\fvrlak\Desktop\at-app-gcp\server\worker\__init__.py:153> exception=JSONDecodeError('Expecting value: line 1 column 1 (char 0)')>
Traceback (most recent call last):
  File "C:\Users\fvrlak\Desktop\at-app-gcp\server\worker\__init__.py", line 160, in run
    self.__apply_trading()
  File "C:\Users\fvrlak\Desktop\at-app-gcp\server\worker\__init__.py", line 144, in __apply_trading
    trade = json.loads(d)
  File "C:\Program Files\Python38\lib\json\__init__.py", line 357, in loads
    return _default_decoder.decode(s)
  File "C:\Program Files\Python38\lib\json\decoder.py", line 337, in decode
    obj, end = self.raw_decode(s, idx=_w(s, 0).end())
  File "C:\Program Files\Python38\lib\json\decoder.py", line 355, in raw_decode
    raise JSONDecodeError("Expecting value", s, err.value) from None
json.decoder.JSONDecodeError: Expecting value: line 1 column 1 (char 0)


https://stackoverflow.com/questions/62476457/python-asycio-runtimewarning-coroutine-was-never-awaited
https://stackoverflow.com/questions/63840674/how-to-handle-runtimewarning-coroutine-new-account-was-never-awaited

            # idea .... take profit as sma and risk as % or units

        if payload == "TEST":
            payload = {
                 "units" : "10000",
                 "stopLoss" : "1.23",
                 "takeProfit" : "1.24",
                 "instrument" : "EUR_USD"
             }

13.1.21
Applying strategy GBP_USD_M15_76bc
Traceback (most recent call last):
  File ".\main.py", line 44, in <module>
    main()
  File ".\main.py", line 39, in main
    worker.run()
  File "C:\Users\fvrlak\Desktop\at-app-gcp\server\worker\__init__.py", line 144, in run
    self.__apply_strategy()
  File "C:\Users\fvrlak\Desktop\at-app-gcp\server\worker\__init__.py", line 77, in __apply_strategy
    self.__create_order(payload = {
  File "C:\Users\fvrlak\Desktop\at-app-gcp\server\worker\__init__.py", line 117, in __create_order
    self.tradeID = x.json()["orderFillTransaction"]["id"]
KeyError: 'orderFillTransaction'

Traceback (most recent call last):
  File ".\main.py", line 44, in <module>
    main()
  File ".\main.py", line 39, in main
    worker.run()
  File "C:\Users\fvrlak\Desktop\at-app-gcp\server\worker\__init__.py", line 147, in run
    self.__apply_trading()
  File "C:\Users\fvrlak\Desktop\at-app-gcp\server\worker\__init__.py", line 131, in __apply_trading
    trade = y.json()['trade']
  File "C:\Users\fvrlak\AppData\Roaming\Python\Python38\site-packages\requests\models.py", line 898, in json
    return complexjson.loads(self.text, **kwargs)
  File "C:\Program Files\Python38\lib\json\__init__.py", line 357, in 
loads
    return _default_decoder.decode(s)
  File "C:\Program Files\Python38\lib\json\decoder.py", line 337, in decode
    obj, end = self.raw_decode(s, idx=_w(s, 0).end())
  File "C:\Program Files\Python38\lib\json\decoder.py", line 355, in raw_decode
    raise JSONDecodeError("Expecting value", s, err.value) from None  
json.decoder.JSONDecodeError: Expecting value: line 1 column 1 (char 0)



{'orderCreateTransaction': {'id': '28621', 'accountID': '101-004-10015616-001', 'userID': 10015616, 'batchID': '28621', 'requestID': '24769913691643754', 'time': '2021-01-14T16:05:30.260024232Z', 'type': 'MARKET_ORDER', 'instrument': 'EUR_USD', 'units': '-5286', 'timeInForce': 
'FOK', 'positionFill': 'DEFAULT', 'takeProfitOnFill': {'price': '1.21400', 'timeInForce': 'GTC'}, 'stopLossOnFill': {'price': '1.21640', 'timeInForce': 'GTC'}, 'reason': 'CLIENT_ORDER'}, 'orderCancelTransaction': 
{'id': '28622', 'accountID': '101-004-10015616-001', 'userID': 10015616, 'batchID': '28621', 'requestID': '24769913691643754', 'time': '2021-01-14T16:05:30.260024232Z', 'type': 'ORDER_CANCEL', 'orderID': '28621', 
'reason': 'LOSING_TAKE_PROFIT'}, 'relatedTransactionIDs': ['28621', '28622'], 'lastTransactionID': '28622'}


File ".\main.py", line 41, in <module>
    main()
  File ".\main.py", line 36, in main
    worker.run()
  File "C:\Users\fvrlak\Desktop\at-app-gcp\server\worker\__init__.py", 
line 175, in run
    self.__apply_trading()
  File "C:\Users\fvrlak\Desktop\at-app-gcp\server\worker\__init__.py", 
line 145, in __apply_trading
    trade = r.json()
  File "C:\Users\fvrlak\AppData\Roaming\Python\Python38\site-packages\requests\models.py", line 898, in json
    return complexjson.loads(self.text, **kwargs)
  File "C:\Program Files\Python38\lib\json\__init__.py", line 357, in loads
    return _default_decoder.decode(s)
  File "C:\Program Files\Python38\lib\json\decoder.py", line 337, in decode
    obj, end = self.raw_decode(s, idx=_w(s, 0).end())
  File "C:\Program Files\Python38\lib\json\decoder.py", line 355, in raw_decode
    raise JSONDecodeError("Expecting value", s, err.value) from None   
json.decoder.JSONDecodeError: Expecting value: line 1 column 1 (char 0)



*************************

import concurrent.futures
import math

PRIMES = [
    112272535095293,
    112582705942171,
    112272535095293,
    115280095190773,
    115797848077099,
    1099726899285419]

def is_prime(n):
    if n < 2:
        return False
    if n == 2:
        return True
    if n % 2 == 0:
        return False

    sqrt_n = int(math.floor(math.sqrt(n)))
    for i in range(3, sqrt_n + 1, 2):
        if n % i == 0:
            return False
    return True

def main():
    with concurrent.futures.ProcessPoolExecutor() as executor:
        for worker in zip(workers, executor.submit(worker.run())):
            print(worker)

if __name__ == '__main__':
    main()


    ***********************************************************
    import concurrent.futures
import urllib.request

URLS = ['http://www.foxnews.com/',
        'http://www.cnn.com/',
        'http://europe.wsj.com/',
        'http://www.bbc.co.uk/',
        'http://some-made-up-domain.com/']

# Retrieve a single page and report the URL and contents
def load_url(url, timeout):
    with urllib.request.urlopen(url, timeout=timeout) as conn:
        return conn.read()

# We can use a with statement to ensure threads are cleaned up promptly
with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
    # Start the load operations and mark each future with its URL
    future_to_url = {executor.submit(load_url, url, 60): url for url in URLS}
    for future in concurrent.futures.as_completed(future_to_url):
        url = future_to_url[future]
        try:
            data = future.result()
        except Exception as exc:
            print('%r generated an exception: %s' % (url, exc))
        else:
            print('%r page is %d bytes' % (url, len(data)))