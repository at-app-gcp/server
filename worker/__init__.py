import json
import numpy as np
from datetime import datetime
import uuid
import pandas as pd
import requests
import asyncio
from api import firestore_db as db

class Worker:
    def __init__(self, instrument, time_frame, index):
        self.instrument = instrument
        self.time_frame = time_frame
        self.index = index
        self.inception = datetime.now()
        self.uid = str(uuid.uuid4())[:4]
        self.trading = False
        self.tradeID = None
        self.workers_countRef = db.collection("SYSTEM").document("broker").get().to_dict()["workers_count"]
        self.candlesRef = db.collection("CANDLES").document(self.instrument).collection(self.time_frame).document("DATA")
        self.accountRef = db.collection("ACCOUNT").document("oanda_practice")
        self.workersRef = db.collection("WORKERS").document(self.instrument).collection(self.time_frame).document("DATA")
        self.errorsRef = db.collection("ERRORS").document(self.__str__())
        self.tradesRef = db.collection("TRADES")


    def __parse_candles(self):
        t, o, h, l, c, v = [],[],[],[],[],[]
        
        for candle in self.candlesRef.get().to_dict()['candles']:
            t.append(candle['time'])
            o.append(float(candle['mid']['o']))
            h.append(float(candle['mid']['h']))
            l.append(float(candle['mid']['l']))
            c.append(float(candle['mid']['c']))
            v.append(candle['volume'])

        return pd.DataFrame(data = ({'time' : t ,'open': o,'high' : h,'low'  : l,'close': c, 'vol' : v}))
    
    def __create_order(self, payload):
            body = {
            "order": {
               "units": payload['units'],
               "stopLossOnFill": {
                 "timeInForce": "GTC",
                 "price": payload["stopLoss"],
               },
               "takeProfitOnFill": {
                   "timeInForce": "GTC",
                 "price": payload["takeProfit"],
               },
               "timeInForce": "FOK",
               "instrument": payload["instrument"],
               "type": "MARKET",
               "positionFill": "DEFAULT",
                },
            }

            headers = {
                "Content-Type": "application/json",
                "Authorization":"Bearer 1d5a4136d5279b04d815c7f736b50ea1-60e8d9e1d1ff67ba601219754891ff5f",
            }

            url = "https://api-fxpractice.oanda.com/v3/accounts/101-004-10015616-001/orders"
            j = json.dumps(body)
            r = requests.post(url,j,headers=headers)

            if r.status_code == 200:
                try :
                    res = r.json()

                except Exception as e:
                    print("EXCEPTION",e)
                    self.errorsRef.set({
                        "workerID" : self.__str__(),
                        "message" : str(e)
                    })
                else:
                    # if res["orderCancelTransaction"]["type"]:
                    print(res, "RESPONSE 200")
                    print(len(res)," LEN RESPONSE 200 ")
                    self.tradeID = res["orderFillTransaction"]["id"]
                    self.trading = True

                # print(res["orderFillTransaction"])
                # print(res["orderCancelTransaction"])
                # if res["orderCancelTransaction"]["type"]:
                #     print("Order Canceled")

                # if res["orderFillTransaction"]:


                # else:
                    # self.tradeID = res["orderFillTransaction"]["id"]
                # print("Error", r.json(), "ABOVE ORDERFIILL TRANSAFCITON")
                # print(self.tradeID, "TEST")
            if r.status_code == 201:
                try :
                    res = r.json()

                except Exception as e:
                    print("EXCEPTION",e)
                    self.errorsRef.set({
                        "workerID" : self.__str__(),
                        "message" : str(e)
                    })
                else:
                    if res["orderCreateTransaction"]["type"] == "MARKET_ORDER":

                        if "orderFillTransaction" in res:
                            self.tradeID = res["orderFillTransaction"]["id"]
                            self.trading = True
                        if "orderCancelTransaction" in res:
                            self.errorsRef.set({
                            "workerID" : self.__str__(),
                            "message" : res["orderCancelTransaction"]["reason"],
                            "time" : str(datetime.now())
                            })
                            pass
                        else:
                            self.errorsRef.set({
                            "workerID" : self.__str__(),
                            "message" : str(res)
                            })
                            pass
                    else:
                        self.errorsRef.set({
                            "workerID" : self.__str__(),
                            "message" : str(res)
                            })
                        pass


                    # print(res, "RESPONSE 201")
                    # print(len(res)," LEN RESPONSE 201")
                    
            
            else:
                print("Error", r.status_code, r)
                self.errorsRef.set({
                        "workerID" : self.__str__(),
                        "message" : str(r)
                    })
                # print(r.json(), " THIS TEST !!!")
    
    def __apply_strategy(self):
        # Variables 
        df = self.__parse_candles()
        price = df.iloc[-1]["close"]
        margin = self.accountRef.get().to_dict()["marginAvailable"]
        units = round(float(margin) / self.workers_countRef) 
        sma = round(sum(df['close']) / len(df),4)
        bb_top = round(abs(2 * np.array(df['close']).std(dtype=np.float64) + sma),4)
        bb_bot = round(abs(2 * np.array(df['close']).std(dtype=np.float64) - sma),4)
        # Strategy
        if price > bb_top:
            stop_loss = round(price + 0.0008,4)
            take_profit = round(price - 0.0016,4) 
            units *= -1
            self.__create_order({
                 "units"      : str(units),
                 "stopLoss"   : str(stop_loss),
                 "takeProfit" : str(take_profit),
                 "instrument" : str(self.instrument)
            })
        
        if price < bb_bot:
            stop_loss = round(price - 0.0008,4)
            take_profit = round(price + 0.00016,4) 
            self.__create_order(payload = {
                 "units"      : str(units),
                 "stopLoss"   : str(stop_loss),
                 "takeProfit" : str(take_profit),
                 "instrument" : str(self.instrument)
            })

        print("Applying strategy", self.__str__())
    
    def __apply_trading(self):
        headers = {
            "Content-Type": "application/json",
            "Authorization":"Bearer 1d5a4136d5279b04d815c7f736b50ea1-60e8d9e1d1ff67ba601219754891ff5f",
        }
        url = "https://api-fxpractice.oanda.com/v3/accounts/101-004-10015616-001/trades/" + self.tradeID
        r = requests.get(url,headers= headers)

        if r.status_code == 200 or 201:
            try:
                trade = r.json()
            except Exception as e:
                print("Ecxeption", e)
                self.errorsRef.set({
                        "workerID" : self.__str__(),
                        "message" : str(e)
                    })

            else:

                if len(trade) == 3:
                    print("trade doent exist",trade)
                
                else:
                    if trade["trade"]['state'] == "OPEN":
                        self.tradesRef.document(self.tradeID).set(trade)
                        print("Trading ...", self.__str__(), self.tradeID)
                    else:
                        self.trading = False
                        self.tradeID = None
                        print("CLOSED TRADE",self.__str__())
            
            # try:
                
            #     print(len(trade), "THIS ONE CHECKKKKKKKK")
            # except Exception as e:
            #     print("exception", e)
            # else:

                
        else:
            print("Error status code",r.status_code, r, datetime.now() )
            self.errorsRef.set({
                        "workerID" : self.__str__(),
                        "message" : str(r)
                    })

    def run(self):
        self.workersRef.set({
                "name": self.__str__(),
                "inception": str(self.inception),
                "status" : "200"
            })
        if self.trading is False:
            self.__apply_strategy()

        if self.trading is True:
            self.__apply_trading()

    def __str__(self):
        r = str(self.instrument) + "_" + \
            str(self.time_frame) + "_" + self.uid
        return r    
